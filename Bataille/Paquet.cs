
using System;

class Paquet
{

    private List<Carte> cartes = new List<Carte>();

    private List<Carte> deck1 = new List<Carte>();
    private List<Carte> deck2 = new List<Carte>();

    public Paquet()
    {
        List<String> ListCouleur = new List<String>(){
                "Trèfle",
                "Carreaux",
                "Coeur",
                "Pique"
            };
        foreach (string Couleur in ListCouleur)
        {
            for (int i = 2; i < 11; i++)
            {
                this.cartes.Add(new Carte(i, i.ToString(), Couleur));
            }
            this.cartes.Add(new Carte(11, "Valet", Couleur));
            this.cartes.Add(new Carte(12, "Dame", Couleur));
            this.cartes.Add(new Carte(13, "Roi", Couleur));
            this.cartes.Add(new Carte(14, "As", Couleur));
        }
    }

    public List<Carte> getDeck1(){
        return this.deck1;
    }
    public List<Carte> getDeck2(){
        return this.deck2;
    }

    public void Shuffle()
    {
        Random rnd = new Random();
        int rnd1 = rnd.Next(20, 500);
        for (int j = 0; j <= rnd1; j++)
        {
            int rnd3 = rnd.Next(0, 51);
            Carte CarteSortie = this.cartes[rnd3];

            this.cartes.RemoveAt(rnd3);
            this.cartes.Add(CarteSortie);
        }
    }


    public void DivideDeck()
    {
        List<Carte> Deck1 = new List<Carte>();
        List<Carte> Deck2 = new List<Carte>();

        for (int i = 0; i < this.cartes.Count / 2; i++)
        {
            Deck1.Add(this.cartes[i]);
        }
        this.deck1 = Deck1;
        for (int i = this.cartes.Count-1; i > (this.cartes.Count-1) / 2; i--)
        {
            Deck2.Add(this.cartes[i]);
        }
        this.deck2 = Deck2;
    }

}