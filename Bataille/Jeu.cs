class Jeu
{

    private List<Carte> deck1 = new List<Carte>();
    private List<Carte> deck2 = new List<Carte>();

    public Jeu(List<Carte> Deck1, List<Carte> Deck2)
    {

        this.deck1 = Deck1;
        this.deck2 = Deck2;

        StartGame();
    }

    public void StartGame()
    {
        Boolean IsGameOn = true;
        int temps=0;
        while (IsGameOn)
        {
            temps++;

            Console.WriteLine(temps);
            Console.WriteLine(this.deck1.Count());
            Console.WriteLine(this.deck2.Count());
            if (this.deck1.Count == 0 || this.deck2.Count == 0)
            {
                IsGameOn = false;
                if (this.deck1.Count == 0)
                {
                    Console.WriteLine("Joueur 1 gagne");
                }
                else if (this.deck2.Count == 0)
                {
                    Console.WriteLine("Joueur 2 gagne");
                }
            }
            else
            {
                Boolean WinnerFound = false;
                while (!WinnerFound)
                {
                    Console.WriteLine();
                    Console.WriteLine("Joueur 1 joue : " + this.deck1[0].getNom() + " de " + this.deck1[0].getCouleur());
                    Console.WriteLine("Joueur 2 joue : " + this.deck2[0].getNom() + " de " + this.deck2[0].getCouleur());
                    if (this.deck1[0].getValeur() > this.deck2[0].getValeur())
                    {

                        Carte Carte1 = this.deck1[0];
                        Carte Carte2 = this.deck2[0];

                        this.deck1.RemoveAt(0);
                        this.deck2.RemoveAt(0);

                        this.deck1.Add(Carte1);
                        this.deck1.Add(Carte2);
                        WinnerFound = true;
                        Console.WriteLine("Joueur 1 gagne le plis");
                    }
                    else if (this.deck1[0].getValeur() < this.deck2[0].getValeur())
                    {
                        Carte Carte1 = this.deck1[0];
                        Carte Carte2 = this.deck2[0];

                        this.deck1.RemoveAt(0);
                        this.deck2.RemoveAt(0);

                        this.deck2.Add(Carte1);
                        this.deck2.Add(Carte2);
                        WinnerFound = true;
                        Console.WriteLine("Joueur 2 gagne le plis");
                    }
                    else if (this.deck1[0].getValeur() == this.deck2[0].getValeur())
                    {
                        Console.WriteLine("Bataille !");
                        List<Carte> PlayedCards = new List<Carte>();

                        PlayedCards.Add(this.deck1[0]);
                        PlayedCards.Add(this.deck2[0]);

                        this.deck1.RemoveAt(0);
                        this.deck2.RemoveAt(0);
                        while (!WinnerFound)
                        {
                            int i = 0;
                            while (this.deck1[0].getValeur() == this.deck2[0].getValeur() && i % 2 == 0)
                            {
                                if (i % 2 == 0)
                                {
                                    Console.WriteLine("Joueur 1 joue : " + this.deck1[0].getNom() + " de " + this.deck1[0].getCouleur());
                                    Console.WriteLine("Joueur 2 joue : " + this.deck2[0].getNom() + " de " + this.deck2[0].getCouleur());
                                    Console.WriteLine("Bataille !");
                                }

                                PlayedCards.Add(this.deck1[0]);
                                PlayedCards.Add(this.deck2[0]);

                                this.deck1.RemoveAt(0);
                                this.deck2.RemoveAt(0);
                                i++;
                            }

                            Console.WriteLine("Joueur 1 joue : " + this.deck1[0].getNom() + " de " + this.deck1[0].getCouleur());
                            Console.WriteLine("Joueur 2 joue : " + this.deck2[0].getNom() + " de " + this.deck2[0].getCouleur());

                            if (this.deck1[i].getValeur() > this.deck2[i].getValeur())
                            {
                                foreach (Carte Carte in PlayedCards)
                                {
                                    this.deck1.Add(Carte);
                                }
                                Console.WriteLine("Joueur 1 gagne le plis");
                                WinnerFound = true;
                            }
                            else
                            {
                                foreach (Carte Carte in PlayedCards)
                                {
                                    this.deck2.Add(Carte);
                                }
                                Console.WriteLine("Joueur 2 gagne le plis");
                                WinnerFound = true;

                            }
                        }
                    }
                }
            }
        }
    }
}