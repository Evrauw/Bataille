using System.Dynamic;

class Carte{
    private readonly int valeur;
    private readonly string nom;
    private readonly string couleur;

    public Carte(int Valeur,String Nom, String Couleur){
        this.valeur=Valeur;
        this.nom=Nom;
        this.couleur=Couleur;
    }

    public int getValeur(){
        return this.valeur;
    }

    public string getNom(){
        return this.nom;
    }

    public string getCouleur(){
        return this.couleur;
    }

}